wait(1)
plr = game.Players.LocalPlayer
char = plr.Character
mouse = plr:GetMouse()
weps = Instance.new("Model", char)
weps.Name = "StealthForceWeapons"
weps.Parent = nil
heldb4 = false
equipped = false
armed = false
modeSelc = 1
modeGun = 1
modeKnife = 1
RH = 0
LH = 0
Create = function(itemClass,tabl)
local item = Instance.new(itemClass)
for i,v in pairs(tabl) do
local a,b = ypcall(function() return item[i] end)
if a then
item[i] = tabl[i]
end
end
return item
end
function runDummyScript(f,scri)
local oldenv = getfenv(f)
local newenv = setmetatable({}, {
__index = function(_, k)
if k:lower() == 'script' then
return scri
else
return oldenv[k]
end
end
})
setfenv(f, newenv)
ypcall(function() f() end)
end
cors = {}
mas = Instance.new("Model",game:GetService("Lighting")) 
mas.Name = "CompiledModel"
o1 = Create("Model",{
["Name"] = "SFWeps",
["Parent"] = mas,
})
o2 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.877022564, 157.010635),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.877022564, 157.010635, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o3 = Create("SpecialMesh",{
["Parent"] = o2,
["Scale"] = Vector3.new(0.47689274, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o4 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.829887688, 156.232101),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387344, 0.829887688, 156.232101, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.536504209, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o5 = Create("SpecialMesh",{
["Parent"] = o4,
["Scale"] = Vector3.new(1, 0.662591994, 0.552160144),
["MeshType"] = Enum.MeshType.Cylinder,
})
o6 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5327682, 0.367500126, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5327682, 0.367500126, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.333824903, 0.200000003, 0.231907219),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o7 = Create("SpecialMesh",{
["Parent"] = o6,
["Scale"] = Vector3.new(1, 0.276079983, 1),
["MeshType"] = Enum.MeshType.Brick,
})
o8 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.665671468, 156.849289),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.665671468, 156.849289, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601725, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o9 = Create("SpecialMesh",{
["Parent"] = o8,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o10 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.665666699, 156.970749),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(97.5387344, 0.665666699, 156.970749, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o11 = Create("SpecialMesh",{
["Parent"] = o10,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o12 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.758104444, 156.497101),
["Rotation"] = Vector3.new(59.9999886, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.758104444, 156.497101, 0, 0, -1, -0.866025388, 0.500000238, 0, 0.500000238, 0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o13 = Create("SpecialMesh",{
["Parent"] = o12,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o14 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.671705365, 156.50267),
["Rotation"] = Vector3.new(104.999939, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.671705365, 156.50267, 0, 0, -1, -0.965926111, -0.25881803, 0, -0.25881803, 0.965926111, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o15 = Create("SpecialMesh",{
["Parent"] = o14,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o16 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.635147691, 156.486099),
["Rotation"] = Vector3.new(120.000015, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.635147691, 156.486099, 0, 0, -1, -0.866025388, -0.500000238, 0, -0.500000238, 0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o17 = Create("SpecialMesh",{
["Parent"] = o16,
["Scale"] = Vector3.new(0.178834781, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o18 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.718729615, 156.508163),
["Rotation"] = Vector3.new(90, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.718729615, 156.508163, 0, 0, -1, -1, 0, 0, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o19 = Create("SpecialMesh",{
["Parent"] = o18,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o20 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["Material"] = Enum.Material.Granite,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.60084641, 156.85791),
["Rotation"] = Vector3.new(-14.9999952, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.60084641, 156.85791, 0, 0, -1, 0.258818984, 0.965925872, 0, 0.965925872, -0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.353382349, 0.220864013),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o21 = Create("SpecialMesh",{
["Parent"] = o20,
["Scale"] = Vector3.new(0.9537853, 1, 1),
["MeshType"] = Enum.MeshType.Brick,
})
o22 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.951361001, 156.01123),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387344, 0.951361001, 156.01123, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o23 = Create("SpecialMesh",{
["Parent"] = o22,
["Scale"] = Vector3.new(0.655727386, 0.662591994, 0.552160144),
["MeshType"] = Enum.MeshType.Cylinder,
})
o24 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5744934, 1.06179368, 156.960953),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5744934, 1.06179368, 156.960953, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o25 = Create("SpecialMesh",{
["Parent"] = o24,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o26 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.620063424, 156.596527),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.620063424, 156.596527, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.298057914, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o27 = Create("SpecialMesh",{
["Parent"] = o26,
["Scale"] = Vector3.new(1, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o28 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.703005433, 156.585495),
["Rotation"] = Vector3.new(45.000042, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.703005433, 156.585495, 0, 0, -1, -0.707107365, 0.707106352, 0, 0.707106352, 0.707107365, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o29 = Create("SpecialMesh",{
["Parent"] = o28,
["Scale"] = Vector3.new(0.298058003, 0.441727817, 0.220864147),
["MeshType"] = Enum.MeshType.Brick,
})
o30 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.879576981, 156.938858),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.879576981, 156.938858, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o31 = Create("SpecialMesh",{
["Parent"] = o30,
["Scale"] = Vector3.new(0.417281151, 0.165647969, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o32 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.4433441, 0.967920601, 157.005127),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(97.4433441, 0.967920601, 157.005127, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o33 = Create("SpecialMesh",{
["Parent"] = o32,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o34 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.4433441, 0.967920601, 156.916779),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(97.4433441, 0.967920601, 156.916779, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o35 = Create("SpecialMesh",{
["Parent"] = o34,
["Scale"] = Vector3.new(0.774950504, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o36 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.4433441, 0.967920601, 156.960953),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(97.4433441, 0.967920601, 156.960953, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o37 = Create("SpecialMesh",{
["Parent"] = o36,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o38 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.4433441, 0.967921555, 156.872589),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(97.4433441, 0.967921555, 156.872589, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o39 = Create("SpecialMesh",{
["Parent"] = o38,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o40 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.951358616, 156.938858),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.951358616, 156.938858, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.202679396, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o41 = Create("SpecialMesh",{
["Parent"] = o40,
["Scale"] = Vector3.new(1, 0.883455932, 0.773024201),
["MeshType"] = Enum.MeshType.Brick,
})
o42 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Dark stone grey"),
["Position"] = Vector3.new(97.4850845, 0.973446667, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.4850845, 0.973446667, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652404, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.388235, 0.372549, 0.384314),
})
o43 = Create("SpecialMesh",{
["Parent"] = o42,
["Scale"] = Vector3.new(1, 0.441727966, 0.38651213),
["MeshType"] = Enum.MeshType.Brick,
})
o44 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.875851929, 156.960953),
["Rotation"] = Vector3.new(-14.9999952, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.875851929, 156.960953, 0, 0, -1, 0.258818984, 0.965925872, 0, 0.965925872, -0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o45 = Create("SpecialMesh",{
["Parent"] = o44,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o46 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.815323651, 156.900223),
["Rotation"] = Vector3.new(-75.000061, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.815323651, 156.900223, 0, 0, -1, 0.965926111, 0.25881803, 0, 0.25881803, -0.965926111, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o47 = Create("SpecialMesh",{
["Parent"] = o46,
["Scale"] = Vector3.new(0.298057973, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o48 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.87072593, 156.938858),
["Rotation"] = Vector3.new(-30.0000114, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.87072593, 156.938858, 0, 0, -1, 0.500000238, 0.866025388, 0, 0.866025388, -0.500000238, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o49 = Create("SpecialMesh",{
["Parent"] = o48,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o50 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.923752606, 156.165848),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.923752606, 156.165848, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.393436462, 0.231907189, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o51 = Create("SpecialMesh",{
["Parent"] = o50,
["Scale"] = Vector3.new(1, 1, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o52 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.785711408, 156.414322),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387344, 0.785711408, 156.414322, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.619960546, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o53 = Create("SpecialMesh",{
["Parent"] = o52,
["Scale"] = Vector3.new(1, 0.220863983, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o54 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.846449673, 156.872589),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.846449673, 156.872589, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o55 = Create("SpecialMesh",{
["Parent"] = o54,
["Scale"] = Vector3.new(0.417281151, 0.496943951, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o56 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5864258, 0.923751652, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5864258, 0.923751652, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652401, 0.231907189, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o57 = Create("SpecialMesh",{
["Parent"] = o56,
["Scale"] = Vector3.new(1, 1, 0.552160144),
["MeshType"] = Enum.MeshType.Brick,
})
o58 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.479126, 0.868534863, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.479126, 0.868534863, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652404, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o59 = Create("SpecialMesh",{
["Parent"] = o58,
["Scale"] = Vector3.new(1, 0.60737592, 0.441728145),
["MeshType"] = Enum.MeshType.Brick,
})
o60 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5029602, 1.06179273, 156.960953),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5029602, 1.06179273, 156.960953, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o61 = Create("SpecialMesh",{
["Parent"] = o60,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o62 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 1.06179368, 156.055405),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 1.06179368, 156.055405, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o63 = Create("SpecialMesh",{
["Parent"] = o62,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o64 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.763624787, 156.618607),
["Rotation"] = Vector3.new(14.9999952, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.763624787, 156.618607, 0, 0, -1, -0.258818984, 0.965925872, 0, 0.965925872, 0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o65 = Create("SpecialMesh",{
["Parent"] = o64,
["Scale"] = Vector3.new(0.298058003, 0.441727817, 0.220864147),
["MeshType"] = Enum.MeshType.Brick,
})
o66 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.6341095, 0.967921555, 157.005112),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(97.6341095, 0.967921555, 157.005112, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o67 = Create("SpecialMesh",{
["Parent"] = o66,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o68 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.6341095, 0.967921555, 156.960953),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(97.6341095, 0.967921555, 156.960953, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o69 = Create("SpecialMesh",{
["Parent"] = o68,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o70 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.6341095, 0.967921555, 156.916779),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(97.6341095, 0.967921555, 156.916779, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o71 = Create("SpecialMesh",{
["Parent"] = o70,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o72 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.566279471, 157.014938),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(97.5387344, 0.566279471, 157.014938, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o73 = Create("SpecialMesh",{
["Parent"] = o72,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o74 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.923751652, 156.701431),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.923751652, 156.701431, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.309980273, 0.231907159, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o75 = Create("SpecialMesh",{
["Parent"] = o74,
["Scale"] = Vector3.new(1, 1, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o76 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.6341095, 0.967922986, 156.872589),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(97.6341095, 0.967922986, 156.872589, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o77 = Create("SpecialMesh",{
["Parent"] = o76,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o78 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5327682, 0.444802344, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5327682, 0.444802344, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.286135644, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o79 = Create("SpecialMesh",{
["Parent"] = o78,
["Scale"] = Vector3.new(1, 0.496943951, 0.828240037),
["MeshType"] = Enum.MeshType.Brick,
})
o80 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.868046582, 156.927811),
["Rotation"] = Vector3.new(-45.000042, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.868046582, 156.927811, 0, 0, -1, 0.707107365, 0.707106352, 0, 0.707106352, -0.707107365, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o81 = Create("SpecialMesh",{
["Parent"] = o80,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o82 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.851877987, 156.916779),
["Rotation"] = Vector3.new(-59.9999886, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.851877987, 156.916779, 0, 0, -1, 0.866025388, 0.500000238, 0, 0.500000238, -0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o83 = Create("SpecialMesh",{
["Parent"] = o82,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o84 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.466890395, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.466890395, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.309980273, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o85 = Create("SpecialMesh",{
["Parent"] = o84,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o86 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.566281378, 156.871368),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.566281378, 156.871368, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.262290955, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o87 = Create("SpecialMesh",{
["Parent"] = o86,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o88 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.665666699, 156.738861),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5387268, 0.665666699, 156.738861, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o89 = Create("SpecialMesh",{
["Parent"] = o88,
["Scale"] = Vector3.new(1, 0.496943951, 0.110432073),
["MeshType"] = Enum.MeshType.Brick,
})
o90 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.665668607, 156.711258),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5387268, 0.665668607, 156.711258, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o91 = Create("SpecialMesh",{
["Parent"] = o90,
["Scale"] = Vector3.new(1, 0.496943951, 0.165648073),
["MeshType"] = Enum.MeshType.Wedge,
})
o92 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.765062451, 156.816147),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5387268, 0.765062451, 156.816147, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o93 = Create("SpecialMesh",{
["Parent"] = o92,
["Scale"] = Vector3.new(0.953785419, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o94 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.765060544, 156.711258),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5387268, 0.765060544, 156.711258, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o95 = Create("SpecialMesh",{
["Parent"] = o94,
["Scale"] = Vector3.new(1, 0.496943951, 0.165648073),
["MeshType"] = Enum.MeshType.Brick,
})
o96 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387344, 0.765060544, 156.92659),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(97.5387344, 0.765060544, 156.92659, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o97 = Create("SpecialMesh",{
["Parent"] = o96,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o98 = Create("Part",{
["Name"] = "G1",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(97.5387268, 0.566279471, 156.738861),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5387268, 0.566279471, 156.738861, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o99 = Create("SpecialMesh",{
["Parent"] = o98,
["Scale"] = Vector3.new(1, 0.496943951, 0.110432073),
["MeshType"] = Enum.MeshType.Wedge,
})
o100 = Create("Part",{
["Name"] = "GlockHandle",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Transparency"] = 1,
["Position"] = Vector3.new(97.5387268, 0.661806047, 156.871368),
["Rotation"] = Vector3.new(90, 0, -0),
["CFrame"] = CFrame.new(97.5387268, 0.661806047, 156.871368, 1, 0, 0, 0, 0, -1, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.261999995, 0.300000012, 0.400000006),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o101 = Create("SpecialMesh",{
["Parent"] = o100,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o102 = Create("Part",{
["Name"] = "GlockHandle2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Transparency"] = 1,
["Position"] = Vector3.new(96.5387497, 0.556109905, 156.871368),
["Rotation"] = Vector3.new(90, 0, -0),
["CFrame"] = CFrame.new(96.5387497, 0.556109905, 156.871368, 1, 0, 0, 0, 0, -1, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.261999995, 0.300000012, 0.400000036),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o103 = Create("SpecialMesh",{
["Parent"] = o102,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o104 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.4791489, 0.762839913, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.4791489, 0.762839913, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652404, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o105 = Create("SpecialMesh",{
["Parent"] = o104,
["Scale"] = Vector3.new(1, 0.60737592, 0.441728145),
["MeshType"] = Enum.MeshType.Brick,
})
o106 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.652407885, 156.497101),
["Rotation"] = Vector3.new(59.9999886, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.652407885, 156.497101, 0, 0, -1, -0.866025388, 0.500000238, 0, 0.500000238, 0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o107 = Create("SpecialMesh",{
["Parent"] = o106,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o108 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.845664799, 156.01123),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387573, 0.845664799, 156.01123, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o109 = Create("SpecialMesh",{
["Parent"] = o108,
["Scale"] = Vector3.new(0.655727386, 0.662591994, 0.552160144),
["MeshType"] = Enum.MeshType.Cylinder,
})
o110 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.773881912, 156.938858),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.773881912, 156.938858, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o111 = Create("SpecialMesh",{
["Parent"] = o110,
["Scale"] = Vector3.new(0.417281151, 0.165647969, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o112 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.551445, 0.903667748, 157.041336),
["Rotation"] = Vector3.new(-180, 0, -0),
["CFrame"] = CFrame.new(96.551445, 0.903667748, 157.041336, 1, 0, 0, 0, -1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.216469988),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o113 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.443367, 0.862223804, 157.005127),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(96.443367, 0.862223804, 157.005127, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o114 = Create("SpecialMesh",{
["Parent"] = o113,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o115 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.52945298, 156.486099),
["Rotation"] = Vector3.new(120.000015, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.52945298, 156.486099, 0, 0, -1, -0.866025388, -0.500000238, 0, -0.500000238, 0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o116 = Create("SpecialMesh",{
["Parent"] = o115,
["Scale"] = Vector3.new(0.178834781, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o117 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.597308934, 156.585495),
["Rotation"] = Vector3.new(45.000042, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.597308934, 156.585495, 0, 0, -1, -0.707107365, 0.707106352, 0, 0.707106352, 0.707107365, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o118 = Create("SpecialMesh",{
["Parent"] = o117,
["Scale"] = Vector3.new(0.298058003, 0.441727817, 0.220864147),
["MeshType"] = Enum.MeshType.Brick,
})
o119 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.724191904, 156.232101),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387573, 0.724191904, 156.232101, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.536504209, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o120 = Create("SpecialMesh",{
["Parent"] = o119,
["Scale"] = Vector3.new(1, 0.662591994, 0.552160144),
["MeshType"] = Enum.MeshType.Cylinder,
})
o121 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.559974968, 156.849289),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.559974968, 156.849289, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601725, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o122 = Create("SpecialMesh",{
["Parent"] = o121,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o123 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.770155907, 156.960953),
["Rotation"] = Vector3.new(-14.9999952, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.770155907, 156.960953, 0, 0, -1, 0.258818984, 0.965925872, 0, 0.965925872, -0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o124 = Create("SpecialMesh",{
["Parent"] = o123,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o125 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.613033891, 156.508163),
["Rotation"] = Vector3.new(90, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.613033891, 156.508163, 0, 0, -1, -1, 0, 0, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o126 = Create("SpecialMesh",{
["Parent"] = o125,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o127 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5745163, 0.956096828, 156.960953),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5745163, 0.956096828, 156.960953, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o128 = Create("SpecialMesh",{
["Parent"] = o127,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o129 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.443367, 0.862223804, 156.916779),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(96.443367, 0.862223804, 156.916779, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o130 = Create("SpecialMesh",{
["Parent"] = o129,
["Scale"] = Vector3.new(0.774950504, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o131 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.443367, 0.862223804, 156.960953),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(96.443367, 0.862223804, 156.960953, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o132 = Create("SpecialMesh",{
["Parent"] = o131,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o133 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["Material"] = Enum.Material.Granite,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.495150983, 156.85791),
["Rotation"] = Vector3.new(-14.9999952, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.495150983, 156.85791, 0, 0, -1, 0.258818984, 0.965925872, 0, 0.965925872, -0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.353382349, 0.220864013),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o134 = Create("SpecialMesh",{
["Parent"] = o133,
["Scale"] = Vector3.new(0.9537853, 1, 1),
["MeshType"] = Enum.MeshType.Brick,
})
o135 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.709627867, 156.900223),
["Rotation"] = Vector3.new(-75.000061, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.709627867, 156.900223, 0, 0, -1, 0.965926111, 0.25881803, 0, 0.25881803, -0.965926111, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o136 = Create("SpecialMesh",{
["Parent"] = o135,
["Scale"] = Vector3.new(0.298057973, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o137 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.818055809, 156.165833),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.818055809, 156.165833, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.393436462, 0.231907189, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o138 = Create("SpecialMesh",{
["Parent"] = o137,
["Scale"] = Vector3.new(1, 1, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o139 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.559970915, 156.970749),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(96.5387573, 0.559970915, 156.970749, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o140 = Create("SpecialMesh",{
["Parent"] = o139,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o141 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.7713269, 157.010635),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.7713269, 157.010635, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o142 = Create("SpecialMesh",{
["Parent"] = o141,
["Scale"] = Vector3.new(0.47689274, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o143 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.845661819, 156.938858),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.845661819, 156.938858, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.202679396, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o144 = Create("SpecialMesh",{
["Parent"] = o143,
["Scale"] = Vector3.new(1, 0.883455932, 0.773024201),
["MeshType"] = Enum.MeshType.Brick,
})
o145 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.680015922, 156.414322),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387573, 0.680015922, 156.414322, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.619960546, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o146 = Create("SpecialMesh",{
["Parent"] = o145,
["Scale"] = Vector3.new(1, 0.220863983, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o147 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5327911, 0.261803985, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5327911, 0.261803985, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.333824903, 0.200000003, 0.231907219),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o148 = Create("SpecialMesh",{
["Parent"] = o147,
["Scale"] = Vector3.new(1, 0.276079983, 1),
["MeshType"] = Enum.MeshType.Brick,
})
o149 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.514366925, 156.596527),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.514366925, 156.596527, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.298057914, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o150 = Create("SpecialMesh",{
["Parent"] = o149,
["Scale"] = Vector3.new(1, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o151 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Dark stone grey"),
["Position"] = Vector3.new(96.4851074, 0.867750823, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.4851074, 0.867750823, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652404, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.388235, 0.372549, 0.384314),
})
o152 = Create("SpecialMesh",{
["Parent"] = o151,
["Scale"] = Vector3.new(1, 0.441727966, 0.38651213),
["MeshType"] = Enum.MeshType.Brick,
})
o153 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.765029907, 156.938858),
["Rotation"] = Vector3.new(-30.0000114, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.765029907, 156.938858, 0, 0, -1, 0.500000238, 0.866025388, 0, 0.866025388, -0.500000238, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o154 = Create("SpecialMesh",{
["Parent"] = o153,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o155 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.740753889, 156.872589),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.740753889, 156.872589, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o156 = Create("SpecialMesh",{
["Parent"] = o155,
["Scale"] = Vector3.new(0.417281151, 0.496943951, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o157 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5864487, 0.818055809, 156.452957),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5864487, 0.818055809, 156.452957, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.22652401, 0.231907189, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o158 = Create("SpecialMesh",{
["Parent"] = o157,
["Scale"] = Vector3.new(1, 1, 0.552160144),
["MeshType"] = Enum.MeshType.Brick,
})
o159 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.443367, 0.862225831, 156.872589),
["Rotation"] = Vector3.new(-90, 90, 0),
["CFrame"] = CFrame.new(96.443367, 0.862225831, 156.872589, 0, 0, 1, -1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o160 = Create("SpecialMesh",{
["Parent"] = o159,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o161 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.361194015, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.361194015, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.309980273, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o162 = Create("SpecialMesh",{
["Parent"] = o161,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o163 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.559970915, 156.738861),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.5387497, 0.559970915, 156.738861, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o164 = Create("SpecialMesh",{
["Parent"] = o163,
["Scale"] = Vector3.new(1, 0.496943951, 0.110432073),
["MeshType"] = Enum.MeshType.Brick,
})
o165 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.460583985, 156.738861),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.5387497, 0.460583985, 156.738861, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o166 = Create("SpecialMesh",{
["Parent"] = o165,
["Scale"] = Vector3.new(1, 0.496943951, 0.110432073),
["MeshType"] = Enum.MeshType.Wedge,
})
o167 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.6341324, 0.862225831, 157.005096),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(96.6341324, 0.862225831, 157.005096, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o168 = Create("SpecialMesh",{
["Parent"] = o167,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o169 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.6341324, 0.862225831, 156.960953),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(96.6341324, 0.862225831, 156.960953, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o170 = Create("SpecialMesh",{
["Parent"] = o169,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o171 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.956096828, 156.055405),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.956096828, 156.055405, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o172 = Create("SpecialMesh",{
["Parent"] = o171,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o173 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.746182919, 156.916779),
["Rotation"] = Vector3.new(-59.9999886, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.746182919, 156.916779, 0, 0, -1, 0.866025388, 0.500000238, 0, 0.500000238, -0.866025388, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o174 = Create("SpecialMesh",{
["Parent"] = o173,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o175 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.818055809, 156.701416),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.818055809, 156.701416, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.309980273, 0.231907159, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o176 = Create("SpecialMesh",{
["Parent"] = o175,
["Scale"] = Vector3.new(1, 1, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o177 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.659365892, 156.92659),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(96.5387573, 0.659365892, 156.92659, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o178 = Create("SpecialMesh",{
["Parent"] = o177,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o179 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.6341324, 0.862225831, 156.916779),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(96.6341324, 0.862225831, 156.916779, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o180 = Create("SpecialMesh",{
["Parent"] = o179,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o181 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387573, 0.460583985, 157.014938),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(96.5387573, 0.460583985, 157.014938, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o182 = Create("SpecialMesh",{
["Parent"] = o181,
["Scale"] = Vector3.new(1, 0.496943951, 0.220864072),
["MeshType"] = Enum.MeshType.Wedge,
})
o183 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.762350917, 156.927811),
["Rotation"] = Vector3.new(-45.000042, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.762350917, 156.927811, 0, 0, -1, 0.707107365, 0.707106352, 0, 0.707106352, -0.707107365, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o184 = Create("SpecialMesh",{
["Parent"] = o183,
["Scale"] = Vector3.new(0.178834811, 0.165648013, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o185 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.6341324, 0.862227857, 156.872589),
["Rotation"] = Vector3.new(-90, -90, 0),
["CFrame"] = CFrame.new(96.6341324, 0.862227857, 156.872589, 0, 0, -1, 1, 0, 0, 0, -1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o186 = Create("SpecialMesh",{
["Parent"] = o185,
["Scale"] = Vector3.new(0.774950683, 0.220863983, 0.110432014),
["MeshType"] = Enum.MeshType.Wedge,
})
o187 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5029831, 0.956096828, 156.960953),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5029831, 0.956096828, 156.960953, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o188 = Create("SpecialMesh",{
["Parent"] = o187,
["Scale"] = Vector3.new(0.298057973, 0.220864043, 0.110432111),
["MeshType"] = Enum.MeshType.Brick,
})
o189 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.460584939, 156.871368),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.460584939, 156.871368, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.262290955, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o190 = Create("SpecialMesh",{
["Parent"] = o189,
["Scale"] = Vector3.new(1, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o191 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.559972942, 156.711258),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.5387497, 0.559972942, 156.711258, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o192 = Create("SpecialMesh",{
["Parent"] = o191,
["Scale"] = Vector3.new(1, 0.496943951, 0.165648073),
["MeshType"] = Enum.MeshType.Wedge,
})
o193 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.659365892, 156.816147),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.659365892, 156.816147, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o194 = Create("SpecialMesh",{
["Parent"] = o193,
["Scale"] = Vector3.new(0.953785419, 0.496943951, 0.99388814),
["MeshType"] = Enum.MeshType.Brick,
})
o195 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.657929897, 156.618607),
["Rotation"] = Vector3.new(14.9999952, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.657929897, 156.618607, 0, 0, -1, -0.258818984, 0.965925872, 0, 0.965925872, 0.258818984, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o196 = Create("SpecialMesh",{
["Parent"] = o195,
["Scale"] = Vector3.new(0.298058003, 0.441727817, 0.220864147),
["MeshType"] = Enum.MeshType.Brick,
})
o197 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5327911, 0.339107037, 156.893463),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5327911, 0.339107037, 156.893463, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.286135644, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o198 = Create("SpecialMesh",{
["Parent"] = o197,
["Scale"] = Vector3.new(1, 0.496943951, 0.828240037),
["MeshType"] = Enum.MeshType.Brick,
})
o199 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.659365892, 156.711258),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.5387497, 0.659365892, 156.711258, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.214601696, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o200 = Create("SpecialMesh",{
["Parent"] = o199,
["Scale"] = Vector3.new(1, 0.496943951, 0.165648073),
["MeshType"] = Enum.MeshType.Brick,
})
o201 = Create("Part",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Fabric,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.7101822, 0.83373183, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.7101822, 0.83373183, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.245639458, 0.675508797, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o202 = Create("BlockMesh",{
["Parent"] = o201,
["Scale"] = Vector3.new(1, 1, 0.153524801),
})
o203 = Create("Part",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Fabric,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.3908463, 0.83373183, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.3908463, 0.83373183, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.245639458, 0.675508797, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o204 = Create("BlockMesh",{
["Parent"] = o203,
["Scale"] = Vector3.new(1, 1, 0.153524801),
})
o205 = Create("Part",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.5505142, 0.83373183, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5505142, 0.83373183, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307049304, 0.798328578, 0.337754577),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o206 = Create("Part",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5505142, 1.70882905, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(97.5505142, 1.70882905, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307049304, 0.951853395, 0.337754577),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o207 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5514374, 1.33960927, 157.101212),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(97.5514374, 1.33960927, 157.101212, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.214934632, 0.291697115),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o208 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5514374, 1.41637456, 156.573074),
["CFrame"] = CFrame.new(97.5514374, 1.41637456, 156.573074, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.368459374, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o209 = Create("SpecialMesh",{
["Parent"] = o208,
["Scale"] = Vector3.new(1, 1, 0.997911215),
["MeshType"] = Enum.MeshType.Wedge,
})
o210 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5514374, 1.41467655, 157.072037),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(97.5514374, 1.41467655, 157.072037, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.368459404, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o211 = Create("SpecialMesh",{
["Parent"] = o210,
["Scale"] = Vector3.new(1, 1, 0.997911155),
["MeshType"] = Enum.MeshType.Wedge,
})
o212 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.5514374, 1.109321, 156.959946),
["Rotation"] = Vector3.new(-180, 0, -0),
["CFrame"] = CFrame.new(97.5514374, 1.109321, 156.959946, 1, 0, 0, 0, -1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.575717986),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o213 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5514374, 1.33960927, 156.527023),
["CFrame"] = CFrame.new(97.5514374, 1.33960927, 156.527023, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.214934632, 0.291697145),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o214 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.5514374, 1.109321, 156.61145),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5514374, 1.109321, 156.61145, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.216469988),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o215 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.5514374, 1.109321, 156.683594),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(97.5514374, 1.109321, 156.683594, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.598746717),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o216 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(97.5514374, 2.30681586, 156.826385),
["CFrame"] = CFrame.new(97.5514374, 2.30681586, 156.826385, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.310120076),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o217 = Create("WedgePart",{
["Name"] = "K1",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(97.5514374, 1.109321, 157.041336),
["Rotation"] = Vector3.new(-180, 0, -0),
["CFrame"] = CFrame.new(97.5514374, 1.109321, 157.041336, 1, 0, 0, 0, -1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.216469988),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o218 = Create("Part",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.5505219, 1.5031749, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5505219, 1.5031749, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307049304, 0.951853395, 0.337754577),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o219 = Create("Part",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Fabric,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.3908539, 0.628078699, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.3908539, 0.628078699, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.245639458, 0.675508797, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o220 = Create("BlockMesh",{
["Parent"] = o219,
["Scale"] = Vector3.new(1, 1, 0.153524801),
})
o221 = Create("Part",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Fabric,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.7101898, 0.628078699, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.7101898, 0.628078699, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.245639458, 0.675508797, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o222 = Create("BlockMesh",{
["Parent"] = o221,
["Scale"] = Vector3.new(1, 1, 0.153524801),
})
o223 = Create("Part",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.5505219, 0.628078699, 156.826996),
["Rotation"] = Vector3.new(-0, -90, 0),
["CFrame"] = CFrame.new(96.5505219, 0.628078699, 156.826996, 0, 0, -1, 0, 1, 0, 1, 0, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307049304, 0.798328578, 0.337754577),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o224 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.551445, 1.20902383, 157.072037),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(96.551445, 1.20902383, 157.072037, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.368459404, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o225 = Create("SpecialMesh",{
["Parent"] = o224,
["Scale"] = Vector3.new(1, 1, 0.997911155),
["MeshType"] = Enum.MeshType.Wedge,
})
o226 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.551445, 1.13395572, 156.527023),
["CFrame"] = CFrame.new(96.551445, 1.13395572, 156.527023, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.214934632, 0.291697145),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o227 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.551445, 0.903667748, 156.683594),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.551445, 0.903667748, 156.683594, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.598746717),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o228 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.551445, 1.21072185, 156.573074),
["CFrame"] = CFrame.new(96.551445, 1.21072185, 156.573074, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.368459374, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o229 = Create("SpecialMesh",{
["Parent"] = o228,
["Scale"] = Vector3.new(1, 1, 0.997911215),
["MeshType"] = Enum.MeshType.Wedge,
})
o230 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.551445, 0.903667748, 156.959946),
["Rotation"] = Vector3.new(-180, 0, -0),
["CFrame"] = CFrame.new(96.551445, 0.903667748, 156.959946, 1, 0, 0, 0, -1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.575717986),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o231 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Position"] = Vector3.new(96.551445, 0.903667748, 156.61145),
["Rotation"] = Vector3.new(-0, 0, -180),
["CFrame"] = CFrame.new(96.551445, 0.903667748, 156.61145, -1, 0, 0, 0, -1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.216469988),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o232 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.551445, 1.13395572, 157.101212),
["Rotation"] = Vector3.new(-180, 0, -180),
["CFrame"] = CFrame.new(96.551445, 1.13395572, 157.101212, -1, 0, 0, 0, 1, 0, 0, 0, -1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.214934632, 0.291697115),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o233 = Create("WedgePart",{
["Name"] = "K2",
["Parent"] = o1,
["Material"] = Enum.Material.Metal,
["BrickColor"] = BrickColor.new("Ghost grey"),
["Position"] = Vector3.new(96.551445, 2.10115957, 156.826385),
["CFrame"] = CFrame.new(96.551445, 2.10115957, 156.826385, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(0.33775425, 0.245639592, 0.310120076),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.792157, 0.796079, 0.819608),
})
o234 = Create("Part",{
["Name"] = "G2",
["Parent"] = o1,
["BrickColor"] = BrickColor.new("Black"),
["Position"] = Vector3.new(96.5387497, 0.566008985, 156.50267),
["Rotation"] = Vector3.new(104.999939, -90, 0),
["CFrame"] = CFrame.new(96.5387497, 0.566008985, 156.50267, 0, 0, -1, -0.965926111, -0.25881803, 0, -0.25881803, 0.965926111, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.200000003, 0.200000003, 0.200000003),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0.105882, 0.164706, 0.207843),
})
o235 = Create("SpecialMesh",{
["Parent"] = o234,
["Scale"] = Vector3.new(0.298057944, 0.220863909, 0.993888199),
["MeshType"] = Enum.MeshType.Brick,
})
o236 = Create("Part",{
["Name"] = "KnifeHandle",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Transparency"] = 1,
["Position"] = Vector3.new(97.5505142, 0.828078628, 156.82782),
["Rotation"] = Vector3.new(90, 0, -0),
["CFrame"] = CFrame.new(97.5505142, 0.828078628, 156.82782, 1, 0, 0, 0, 0, -1, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307000011, 0.300000012, 0.737999976),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
o237 = Create("Part",{
["Name"] = "KnifeHandle2",
["Parent"] = o1,
["Material"] = Enum.Material.Wood,
["BrickColor"] = BrickColor.new("Rust"),
["Transparency"] = 1,
["Position"] = Vector3.new(96.5505219, 0.622406721, 156.826996),
["Rotation"] = Vector3.new(90, 0, -0),
["CFrame"] = CFrame.new(96.5505219, 0.622406721, 156.826996, 1, 0, 0, 0, 0, -1, 0, 1, 0),
["CanCollide"] = false,
["Size"] = Vector3.new(0.307000011, 0.300000012, 0.737999976),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.560784, 0.298039, 0.164706),
})
mas.Parent = workspace
mas:MakeJoints()
local mas1 = mas:GetChildren()
for i=1,#mas1 do
	mas1[i].Parent = workspace 
	ypcall(function() mas1[i]:MakeJoints() end)
end
mas:Destroy()
for i=1,#cors do
coroutine.resume(cors[i])
end
glockpeices = game.Workspace.SFWeps:GetChildren()
for i, v in pairs(glockpeices) do
	if v.Name == "G1" or v.Name == "GlockHandle" then
		v.Parent = weps
	end
end
for i = 1, #glockpeices do
	if glockpeices[i].Name == "G1" then
			w = Instance.new("Weld", glockpeices[i])
			w.Part0 = weps.GlockHandle
			w.Part1 = glockpeices[i]
			w.C0 = weps.GlockHandle.CFrame:inverse()
			w.C1 = glockpeices[i].CFrame:inverse()
	end
end
glockpeices2 = game.Workspace.SFWeps:GetChildren()
for i, v in pairs(glockpeices2) do
	if v.Name == "G2" or v.Name == "GlockHandle2" then
		v.Parent = weps
	end
end
for i = 1, #glockpeices2 do
	if glockpeices2[i].Name == "G2" then
		w = Instance.new("Weld", glockpeices2[i])
		w.Part0 = weps.GlockHandle2
		w.Part1 = glockpeices2[i]
		w.C0 = weps.GlockHandle2.CFrame:inverse()
		w.C1 = glockpeices2[i].CFrame:inverse()
	end
end
knifepeices = game.Workspace.SFWeps:GetChildren()
for i, v in pairs(knifepeices) do
	if v.Name == "K1" or v.Name == "KnifeHandle" then
		v.Parent = weps
	end
end
for i = 1, #knifepeices do
	if knifepeices[i].Name == "K1" then
		w = Instance.new("Weld", knifepeices[i])
		w.Part0 = weps.KnifeHandle
		w.Part1 = knifepeices[i]
		w.C0 = weps.KnifeHandle.CFrame:inverse()
		w.C1 = knifepeices[i].CFrame:inverse()
	end
end
knifepeices2 = game.Workspace.SFWeps:GetChildren()
for i, v in pairs(knifepeices2) do
	if v.Name == "K2" or v.Name == "KnifeHandle2" then
		v.Parent = weps
	end
end
for i = 1, #knifepeices2 do
	if knifepeices2[i].Name == "K2" then
		w = Instance.new("Weld", knifepeices2[i])
		w.Part0 = weps.KnifeHandle2
		w.Part1 = knifepeices2[i]
		w.C0 = weps.KnifeHandle2.CFrame:inverse()
		w.C1 = knifepeices2[i].CFrame:inverse()
	end
end
game.Workspace.SFWeps:remove()
function toolhold()
	if heldb4 == true then
		char.Torso.RightArmAnimation.C1 = CFrame.new(-1.5, .5, .5) * CFrame.Angles(math.rad(-90), 0 ,0)
		char.Torso.LeftArmAnimation.C1 = CFrame.new(1.5, .5, .5) * CFrame.Angles(math.rad(-90), 0 ,0)
		weps.Parent = char
		local G1 = Instance.new("Weld", char:FindFirstChild("Right Arm"))
		G1.Name = "Glock1Weld"
		G1.Part0 = char:FindFirstChild("Right Arm")
		G1.Part1 = weps.GlockHandle
		G1.C1 = CFrame.new(0, 1 ,0)
		local G2 = Instance.new("Weld", char:FindFirstChild("Left Arm"))
		G2.Name = "Glock2Weld"
		G2.Part0 = char:FindFirstChild("Left Arm")
		G2.Part1 = weps.GlockHandle2
		G2.C1 = CFrame.new(0, 1 ,0)
		local K1 = Instance.new("Weld", char:FindFirstChild("Right Arm"))
		K1.Name = "Knife1Weld"
		K1.Part0 = char:FindFirstChild("Right Arm")
		K1.Part1 = weps.KnifeHandle
		K1.C1 = CFrame.new(0, 1 ,0)
		local K2 = Instance.new("Weld", char:FindFirstChild("Left Arm"))
		K2.Name = "Knife2Weld"
		K2.Part0 = char:FindFirstChild("Left Arm")
		K2.Part1 = weps.KnifeHandle2
		K2.C1 = CFrame.new(0, 1 ,0)
		if RH == 1 then
			for i, v in pairs(glockpeices) do
				if v.Name == "G1" then
					v.Transparency = 0
				end
			end
		else
			for i, v in pairs(glockpeices) do
				if v.Name == "G1" then
					v.Transparency = 1
				end
			end
		end
		if RH == 2 then
			for i, v in pairs(knifepeices) do
				if v.Name == "K1" then
					v.Transparency = 0
				end
			end
		else
			for i, v in pairs(knifepeices) do
				if v.Name == "K1" then
					v.Transparency = 1
				end
			end
		end
		if LH == 1 then
			for i, v in pairs(glockpeices2) do
				if v.Name == "G2" then
					v.Transparency = 0
				end			
			end
		else
			for i, v in pairs(glockpeices2) do
				if v.Name == "G2" then
					v.Transparency = 1
				end			
			end
		end
		if LH == 2 then
			for i, v in pairs(knifepeices2) do
				if v.Name == "K2" then
					v.Transparency = 0
				end			
			end
		else
			for i, v in pairs(knifepeices2) do
				if v.Name == "K2" then
					v.Transparency = 1
				end			
			end
		end
	end
	if heldb4 == false then
		weps.Parent = char
		local RAanim = Instance.new("Weld", char.Torso)
		RAanim.Name = "RightArmAnimation"
		RAanim.Part0 = char.Torso
		RAanim.Part1 = char:FindFirstChild("Right Arm")
		RAanim.C1 = CFrame.new(-1.5, .5, .5) * CFrame.Angles(math.rad(-90), 0, 0)
		local LAanim = Instance.new("Weld", char.Torso)
		LAanim.Name = "LeftArmAnimation"
		LAanim.Part0 = char.Torso
		LAanim.Part1 = char:FindFirstChild("Left Arm")
		LAanim.C1 = CFrame.new(1.5, .5, .5) * CFrame.Angles(math.rad(-90), 0, 0)
		local G1 = Instance.new("Weld", char:FindFirstChild("Right Arm"))
		G1.Name = "Glock1Weld"
		G1.Part0 = char:FindFirstChild("Right Arm")
		G1.Part1 = weps.GlockHandle
		G1.C1 = CFrame.new(0, 1 ,0)
		local G2 = Instance.new("Weld", char:FindFirstChild("Left Arm"))
		G2.Name = "Glock2Weld"
		G2.Part0 = char:FindFirstChild("Left Arm")
		G2.Part1 = weps.GlockHandle2
		G2.C1 = CFrame.new(0, 1 ,0)
		local K1 = Instance.new("Weld", char:FindFirstChild("Right Arm"))
		K1.Name = "Knife1Weld"
		K1.Part0 = char:FindFirstChild("Right Arm")
		K1.Part1 = weps.KnifeHandle
		K1.C1 = CFrame.new(0, 1 ,0)
		local K2 = Instance.new("Weld", char:FindFirstChild("Left Arm"))
		K2.Name = "Knife2Weld"
		K2.Part0 = char:FindFirstChild("Left Arm")
		K2.Part1 = weps.KnifeHandle2
		K2.C1 = CFrame.new(0, 1 ,0)
		for i, v in pairs(glockpeices2) do
			if v.Name == "G2" or v.Name == "GlockHandle2" then
				v.Transparency = 1
			end
		end
		for i, v in pairs(knifepeices) do
			if v.Name == "K1" or v.Name == "KnifeHandle" then
				v.Transparency = 1
			end
		end
		RH = 1
		LH = 2
		heldb4 = true
		equipped = true
	end
end
function untoolhold()
	char.Torso.RightArmAnimation.C1 = CFrame.new(-1.5, 0, 0) * CFrame.Angles(0, 0 ,0)
	char.Torso.LeftArmAnimation.C1 = CFrame.new(1.5, 0, 0) * CFrame.Angles(0, 0 ,0)
	char:FindFirstChild("Right Arm").Glock1Weld:remove()
	char:FindFirstChild("Right Arm").Knife1Weld:remove()
	char:FindFirstChild("Left Arm").Glock2Weld:remove()
	char:FindFirstChild("Left Arm").Knife2Weld:remove()
	weps.Parent = nil
	equipped = false
end
function shoot()
	
end
function key(key)
	if key == "c" then
		if armed == false then
			toolhold()
			armed = true
		else
			untoolhold()
			armed = false
		end
	elseif key == "q" then
		if modeSelc == 1 then
			if modeKnife == 3 then return false end
			modeKnife = modeKnife + 1
			local changeTo = nil
			local gui = Instance.new("ScreenGui",plr.PlayerGui)
			local frame = Instance.new("Frame",gui)
			local txt = Instance.new("TextLabel",frame)
			frame.Size = UDim2.new(1,0,0.1,0)
			frame.Style = "DropShadow"
			frame.Position = UDim2.new(0,0,-0.2,0)
			txt.Size = UDim2.new(1,0,1,0)
			txt.Font = "ArialBold"
			txt.TextColor3 = Color3.new(200,200,200)
			txt.TextScaled = true
			if modeKnife == 2 then
				changeTo = "Grab"
			elseif modeKnife == 3 then
				changeTo = "Throw"
			end
			if changeTo == nil then
				txt.Text = "You cant change the mode to nil"
			else
				txt.Text = "Knife mode changed to : "..changeTo
			end	
			txt.BackgroundTransparency = 1
			frame.Visible = true
			txt.Visible = true
			frame:TweenPosition(UDim2.new(0,0,0,0),"Out","Back")
			wait(3)
			frame:TweenPosition(UDim2.new(0,0,-0.2,0),"In","Back")
			wait(3)
			frame.Visible = false
			gui:remove()
			-- forward Knife
		else
			if modeGun == 3 then return false end
			modeGun = modeGun + 1
			local changeTo = nil
			local gui = Instance.new("ScreenGui",plr.PlayerGui)
			local frame = Instance.new("Frame",gui)
			local txt = Instance.new("TextLabel",frame)
			frame.Size = UDim2.new(1,0,0.1,0)
			frame.Style = "DropShadow"
			frame.Position = UDim2.new(0,0,-0.2,0)
			txt.Size = UDim2.new(1,0,1,0)
			txt.Font = "ArialBold"
			txt.TextColor3 = Color3.new(200,200,200)
			txt.TextScaled = true
			if modeGun == 2 then
				changeTo = "Automatic"
			elseif modeGun == 3 then
				changeTo = "Laser"
			end
			if changeTo == nil then
				txt.Text = "You cant change the mode to nil"
			else
				txt.Text = "Gun mode changed to : "..changeTo
			end	
			txt.BackgroundTransparency = 1
			frame.Visible = true
			txt.Visible = true
			frame:TweenPosition(UDim2.new(0,0,0,0),"Out","Back")
			wait(3)
			frame:TweenPosition(UDim2.new(0,0,-0.2,0),"In","Back")
			wait(3)
			frame.Visible = false
			gui:remove()
			-- forward Gun
		end
	elseif key == "e" then
		if modeSelc == 1 then
			if modeKnife == 1 then return false end
			modeKnife = modeKnife - 1
			local changeTo = nil
			local gui = Instance.new("ScreenGui",plr.PlayerGui)
			local frame = Instance.new("Frame",gui)
			local txt = Instance.new("TextLabel",frame)
			frame.Size = UDim2.new(1,0,0.1,0)
			frame.Style = "DropShadow"
			frame.Position = UDim2.new(0,0,-0.2,0)
			txt.Size = UDim2.new(1,0,1,0)
			txt.Font = "ArialBold"
			txt.TextColor3 = Color3.new(200,200,200)
			txt.TextScaled = true
			if modeKnife == 1 then
				changeTo = "Slash"
			elseif modeKnife == 2 then
				changeTo = "Grab"
			end
			if changeTo == nil then
				txt.Text = "You cant change the mode to nil"
			else
				txt.Text = "Knife mode changed to : "..changeTo
			end			
			txt.BackgroundTransparency = 1
			frame.Visible = true
			txt.Visible = true
			frame:TweenPosition(UDim2.new(0,0,0,0),"Out","Back")
			wait(3)
			frame:TweenPosition(UDim2.new(0,0,-0.2,0),"In","Back")
			wait(3)
			frame.Visible = false
			gui:remove()
			-- back Knife
		else
			if modeGun == 1 then return false end
			modeGun = modeGun - 1
			local changeTo = nil
			local gui = Instance.new("ScreenGui",plr.PlayerGui)
			local frame = Instance.new("Frame",gui)
			local txt = Instance.new("TextLabel",frame)
			frame.Size = UDim2.new(1,0,0.1,0)
			frame.Style = "DropShadow"
			frame.Position = UDim2.new(0,0,-0.2,0)
			txt.Size = UDim2.new(1,0,1,0)
			txt.Font = "ArialBold"
			txt.TextColor3 = Color3.new(200,200,200)
			txt.TextScaled = true
			if modeGun == 1 then
				changeTo = "Semi Automatic"
			elseif modeGun == 2 then
				changeTo = "Automatic"
			end
			if changeTo == nil then
				txt.Text = "You cant change the mode to nil"
			else
				txt.Text = "Gun mode changed to : "..changeTo
			end				
			txt.BackgroundTransparency = 1
			frame.Visible = true
			txt.Visible = true
			frame:TweenPosition(UDim2.new(0,0,0,0),"Out","Back")
			wait(3)
			frame:TweenPosition(UDim2.new(0,0,-0.2,0),"In","Back")
			wait(3)
			frame.Visible = false
			gui:remove()
			-- back Gun
		end
	elseif key == "g" then
		modeSelc = 2
	elseif key == "k" then
		modeSelc = 1
	end
end
mouse.KeyDown:connect(key)